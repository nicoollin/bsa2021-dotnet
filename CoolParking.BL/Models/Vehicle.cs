﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex pattern = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            bool match = pattern.IsMatch(id);
            if(!match)
            {
                throw new ArgumentException("Id has a wrong format");
            }

            Id = id;
            VehicleType = vehicleType;
            this.Balance = balance;

            if (balance < 0)
            {
                throw new ArgumentException("Balance could not be <0");
            }
            
        }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }
    }
}
