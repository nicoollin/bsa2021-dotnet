﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }
       
        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            
        }

        public void Start()
        {
            
        }

        public void Stop()
        {
           
        }
    }
}